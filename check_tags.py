import pydicom

def check_tags(file_path, tags):
    file = pydicom.read_file(file_path)
    return {tag: True if tag in file else False for tag in tags}

#check the dictionary
def check_dict(dict):
    if False in dict.values():
        return False
    return True

check_dict(check_tags('/home/zaschke/Downloads/dicoms/N2D_0001.dcm',  [('0008', '0032'), ('0010', '1030'), ('0018', '1074'), ('0018', '1075'), ('0018', '1072'), ('0054', '1001'), ('0028', '1052'), ('0028','1053')]))
